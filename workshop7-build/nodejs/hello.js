var http = require('http');

//create a server object:
http.createServer(function (req, res) {
  res.write('Hello World! Docker Container Nodejs');
  res.end();
}).listen(4444);

console.log('Server running at port 4444');