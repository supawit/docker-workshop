var http = require('http');

//create a server object:
var server = http.createServer(function (req, res) {
  res.write('Hello World! Docker Container Nodejs App Node 1');
  res.write(JSON.stringify(req.headers));
  res.end();
}).listen(4444);

console.log('Server running at port 4444');
